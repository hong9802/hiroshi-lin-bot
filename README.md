# DiscordBOT

## Before...
```bash
$ psql -f create_db.sql
```
You need to postgresql setup!

## How to use?
@Hiroshi Lin "function" "etc... options"
ex) @Hiroshi Lin ping

## Functions.
* ping : responce Pong!
* help : help
* info : get Host Computer info
* play [file's local link] : play music on local link file
* stream [youtube urll] : play youtube stream music
* tts [send content] : send on your voice channel(Kakao API used.)
* rotation : send League_of_Legends KOR server rotation champion  
* summoner [your League of Legends id] : send solo rank tier, 5vs5 rank + solo rank winrate
* wiki [content] : searched on Wikipedia and send.(PS. if content is random It get random article)
* moon [location] : get moon rise & set info(서울/경기/강원/충북/충남/전북/전남/경북/경남/제주/대전/부산/인천 등... 최소 광역시 이상)

## PS.
This bot based on [lagbot](https://github.com/sgtlaggy/lagbot)