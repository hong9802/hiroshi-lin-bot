from discord.ext import commands
import utils.champion
import requests
import config
import json

class LOL(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def summoner(self, ctx, *names):
        name = ""
        for i in range(0, len(names), 1):
            name += names[i]
        url = "https://kr.api.riotgames.com/lol/summoner/v4/summoners/by-name/{}".format(name)
        headers = {
            "Origin": "https://developer.riotgames.com",
            "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
            "X-Riot-Token": "{}".format(config.lol_token),
            "Accept-Language": "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36"
        }
        response = requests.get(url = url, headers = headers)
        if(response.status_code != 200):
            await ctx.send("response error")
            return
        encrypted_id = response.json()['id']
        url = "https://kr.api.riotgames.com/lol/league/v4/entries/by-summoner/{}".format(encrypted_id)
        response = requests.get(url = url, headers = headers)
        if(response.status_code != 200):
            await ctx.send("response error")
            return
        league_dicts = response.json()
        queueType = {}
        win = 0 
        lose = 0
        if(len(league_dicts) == 0):
            msg = "{} 님은 언랭이라 확인할 수 없음".format(name)
            await ctx.send(msg)
            return
        for i in range(0, len(league_dicts), 1):
            if(league_dicts[i]['queueType'] != "RANKED_TFT"):
                queueType[league_dicts[i]['queueType']] = i
                win += league_dicts[i]['wins']
                lose += league_dicts[i]['losses']
        winrate = win/(win+lose)*100
        winrate = round(winrate, 2)
        print(queueType)
        msg = "{} 의 랭크 : {} {}, 승률 : {}%, 총 판수 : {}".format(name, league_dicts[queueType['RANKED_SOLO_5x5']]['tier'], league_dicts[queueType['RANKED_SOLO_5x5']]['rank'], winrate, win+lose)
        await ctx.send(msg)

    @commands.command()
    async def rotation(self, ctx):
        url = "https://kr.api.riotgames.com/lol/platform/v3/champion-rotations"
        headers = {
            "Origin": "https://developer.riotgames.com",
            "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
            "X-Riot-Token": "{}".format(config.lol_token),
            "Accept-Language": "ko-KR,ko;q=0.9,en-US;q=0.8,en;q=0.7",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36"
        }
        response = requests.get(url, headers=headers)
        if(response.status_code != 200):
            await ctx.send("response error")
            return
        champion_tmp = json.loads(response.text)
        champion_tmp = champion_tmp['freeChampionIds']
        msg = ""
        for i in range(0, len(champion_tmp), 1):
            msg += utils.champion.convert(champion_tmp[i])
            if(i != len(champion_tmp)-1):
                msg += ", "
        await ctx.send(msg)


def setup(bot):
    bot.add_cog(LOL(bot))