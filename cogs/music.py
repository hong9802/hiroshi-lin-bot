from discord.ext import commands
import asyncio
import youtube_dl
import discord
import config
import requests
import os
import glob

# Suppress noise about console usage from errors
youtube_dl.utils.bug_reports_message = lambda: ''


ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)
g_volume = 7/100
class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume = g_volume):
        super().__init__(source, volume)
        self.data = data
        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream = False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            #take first item from a playlist
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)

sayed_id = ""
voicectx = None

class Tts():
    @classmethod
    async def request_TTS(cls, says):
        url = "https://kakaoi-newtone-openapi.kakao.com/v1/synthesize"
        headers = {
            'Content-Type': 'application/xml',
            'Authorization': 'KakaoAK {}'.format(config.kakao_token),
        }
        data = """<speak><voice>{}</voice></speak>""".format(says).encode('utf-8')
        response = requests.post('https://kakaoi-newtone-openapi.kakao.com/v1/synthesize', headers=headers, data=data)
        with open('response.mp3', 'wb') as file:
            file.write(response.content)

class Music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def say_enable(self, ctx):
        global sayed_id
        global voicectx 
        if(sayed_id is ""):
            sayed_id = ctx.message.author.id
            voicectx = ctx
 
    @commands.command()
    async def tts(self, ctx, *say_commands):
        filepath = os.getcwd() +"/response.mp3"
        sayed = "" 
        for say in say_commands:    
            sayed += say + " "
        await Tts.request_TTS(sayed)
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio('response.mp3'))
        ctx.voice_client.play(source, after=lambda e: print('Player error: %s' % e) if e else None)

    @commands.command()
    async def join(self, ctx, *, channel: discord.VoiceChannel):
        """Joins a voice channel"""
        if ctx.voice_client is not None:
            return await ctx.voice_client.move_to(channel)
        await channel.connect()

    @commands.command()
    async def play(self, ctx, *, query):
        """Plays a file from the local filesystem"""
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(query))
        ctx.voice_client.play(source, after=lambda e: print('Player error: %s' % e) if e else None)
        await ctx.send('Now playing: {}'.format(query))

    @commands.command()
    async def yt(self, ctx, *, url):
        """Plays from a url (almost anything youtube_dl supports)"""
        async with ctx.typing():
            player = await YTDLSource.from_url(url, loop=self.bot.loop)
            ctx.voice_client.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
        await ctx.send('Now playing: {}'.format(player.title))

    @commands.command()
    async def stream(self, ctx, *, url):
        """Streams from a url (same as yt, but doesn't predownload)"""
        async with ctx.typing():
            player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
            ctx.voice_client.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
        await ctx.send('Now playing: {}'.format(player.title))

    @commands.command()
    async def volume(self, ctx, volume: int):
        """Changes the player's volume"""
        if ctx.voice_client is None:
            return await ctx.send("Not connected to a voice channel.")
        global g_volume
        g_volume = volume/100 
        ctx.voice_client.source.volume = volume/100
        await ctx.send("Changed volume to {}%".format(volume))

    @commands.command()
    async def stop(self, ctx):
        """Stops and disconnects the bot from voice"""
        await ctx.voice_client.disconnect()
        files = glob.glob("*")
        for f in files:
            if(f.endswith(".webm") or f.endswith(".m4a")):
                os.remove(f) 
     
    @play.before_invoke
    @yt.before_invoke
    @stream.before_invoke
    @tts.before_invoke 
    @say_enable.before_invoke 
    async def ensure_voice(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
            else:
                await ctx.send("You are not connected to a voice channel.")
                raise commands.CommandError("Author not connected to a voice channel.")
        elif ctx.voice_client.is_playing():
            ctx.voice_client.stop()

    @commands.Cog.listener()
    async def on_message(self, message):
        global sayed_id
        global voicectx 
        if(message.author.id is sayed_id):
            await self.tts(voicectx, message.content)

def setup(bot):
    bot.add_cog(Music(bot))
