from discord.ext import commands
from datetime import datetime
import xml.etree.ElementTree as ET
import utils.moonConvert
import config
import requests
import subprocess

class Basic(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command(name='ping')
    async def ping_command(self, ctx):
        msg = await ctx.send(content = 'Pinging')
        await msg.edit(content="Pong!")
        return

    @commands.command(name='info')
    async def system_info(self, ctx):
        """Get Host Computer Info"""
        msg = subprocess.check_output(["uname -a"], shell = True)
        msg = msg[0:-1]
        await ctx.send(content=msg)

    @commands.command(name='moon')
    async def moon(self, ctx, location):
        """Get Moon rise & set"""
        url = "http://apis.data.go.kr/B090041/openapi/service/RiseSetInfoService/getAreaRiseSetInfo?serviceKey=" + config.moonapi + "&locdate=" + str(datetime.now().strftime("%Y%m%d")) + "&location=" + utils.moonConvert.convert(location)
        r = requests.get(url)
        moon = ET.fromstring(r.text)
        for moon_child in moon.iter():
            if(moon_child.tag == "moonrise"):
                moonrise = moon_child.text
            elif(moon_child.tag == "moonset"):
                moonset = moon_child.text
        moonrise = str(moonrise)[:2] + ":" + str(moonrise)[2:]
        moonset = str(moonset)[:2] + ":" + str(moonset)[2:]
        content = location + " 달 관찰시간 월출 " + moonrise + "월몰 " + moonset
        await ctx.send(content)
 
    @commands.command(name='game')
    async def game_changer(self, ctx, *game_name):
        """Change playing game"""
        games = ""
        for i in range(0, len(game_name), 1):
            games += game_name[i] + " "
        await self.bot.set_game(games)
        await ctx.send(content="finish")

    @commands.Cog.listener()
    async def on_message(self, message):
        if(message.content.startswith("<@253024615285129227>")):
            await message.channel.send("기만질좀 그만하세요 기만러님")

def setup(bot):
    bot.add_cog(Basic(bot))
