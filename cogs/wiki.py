from discord.ext import commands
from bs4 import BeautifulSoup
import discord
import requests
import re

class Wiki(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def wiki(self, ctx, *targets):
        """Get Data on WikiPedia"""
        target = ""
        if(targets[0] == "random"):
            target = "%ED%8A%B9%EC%88%98:%EC%9E%84%EC%9D%98%EB%AC%B8%EC%84%9C"
        else : 
            for i in range(0, len(targets), 1):
                target += targets[i] + "_"
        try:
            url = "https://ko.wikipedia.org/wiki/{}".format(target)
            r = requests.get(url)
            soup = BeautifulSoup(r.text, "html.parser")
            div_finder = soup.find("div", class_="mw-parser-output")
            tmp_content = div_finder.find_all(['p', 'ul'])
            content = ""
            for i in range(0, len(tmp_content), 1):
                if(tmp_content[i].parent.get('id') == "toc"):
                    break
                if(tmp_content[i].parent.name != "td" or tmp_content[i].parent.name != "li"):
                    content += str(tmp_content[i])
                    content = re.sub('<.+?>', '', content, 0).strip()
                    content = re.sub('{.+?}', '', content, 0).strip()
                    content += "\n"
            if(len(content) > 2048):
                content = content[0:2044] + "..."
            embed = discord.Embed(
                titile = target,
                color = 0xFFFFFF,
                description = content
            )
            embed.set_author(
                name = "WikiPedia",
                icon_url="https://upload.wikimedia.org/wikipedia/en/thumb/8/80/Wikipedia-logo-v2.svg/1024px-Wikipedia-logo-v2.svg.png",
                url=url
            )
            await ctx.send(embed=embed)
        except AttributeError as e:
            await ctx.send(target[0:-1] + " 문서가 존재하지 않습니다.")

def setup(bot):
    bot.add_cog(Wiki(bot))
