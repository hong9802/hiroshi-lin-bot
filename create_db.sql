CREATE ROLE hiroshibot WITH PASSWORD 'password';
CREATE DATABASE hiroshibot WITH OWNER hiroshibot;

\c hiroshibot hiroshibot

CREATE TABLE prefixes (
    guild_id bigint PRIMARY KEY,
    prefix text,
    allow_default boolean
);
CREATE TABLE newrole (
    guild_id bigint PRIMARY KEY,
    role_id bigint,
    autoremove boolean DEFAULT FALSE,
    autoadd boolean DEFAULT TRUE
);