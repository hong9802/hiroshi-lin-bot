import asyncio
import logging
import sys
from hiroshiBot import HiroshiBot

try:
    import uvloop
except ImportError:
    pass
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

debug = any('debug' in arg.lower() for arg in sys.argv)
logging.basicConfig(level = logging.INFO if debug else logging.WARNING)

help_attrs = {'hidden' : True}
inital_cogs = ['cogs.basic', 'cogs.music', 'cogs.lol', 'cogs.wiki']

if not debug:
    inital_cogs.append('cogs.botlist')

if __name__ == '__main__':
    bot = HiroshiBot(help_attrs, debug=debug)
    for cog in inital_cogs:
        try:
            bot.load_extension(cog)
        except Exception as e:
            logging.exception(f"Couldn't load cog {cog}")
    
    status = bot.run()
    logging.critical(f'Exiting with {status}')
    logging.shutdown()
    sys.exit(status)